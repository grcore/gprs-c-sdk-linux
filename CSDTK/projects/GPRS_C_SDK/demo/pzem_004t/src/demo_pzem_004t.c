//
// Author : Goutham Rapolu
// Date : 05/18/2022
// Description : Works with PZEM-004T V3 energy meter module. Connect energy meter 'Tx' & 'Rx' to A9 module 'Rx2' & 'Tx2' pins.
//
#include <api_os.h>
#include <api_hal_uart.h>
#include <api_debug.h>
#include <time.h>

#define PZEM_DEFAULT_ADDR 0xF8
#define PZEM_BAUD_RATE 9600

#define CMD_RHR 0x03
#define CMD_RIR 0X04
#define CMD_WSR 0x06
#define CMD_CAL 0x41
#define CMD_REST 0x42

#define WREG_ALARM_THR 0x0001
#define WREG_ADDR 0x0002

#define INVALID_ADDRESS 0x00

#define MAIN_TASK_STACK_SIZE (1024 * 2)
#define MAIN_TASK_PRIORITY 0
#define MAIN_TASK_NAME "PZEM Main Task"

uint8_t _addr; // Module address
uint8_t data[26];
uint32_t _update = 0;

static HANDLE pzem_004tTaskHandle = NULL;

struct
{
    float voltage;
    float current;
    float power;
    float energy;
    float frequency;
    float pf;
} _pzem;                                    // Measured values

void send8bc(uint8_t cmd, uint16_t rAddr, uint16_t val, bool check, uint16_t slave_addr) // Send 8 byte command
{
    uint8_t sendBuf[9];                  // Define Send buffer
    if ((slave_addr == 0xFFFF) ||
        (slave_addr < 0x01) ||
        (slave_addr > 0xF7))
    {
        slave_addr = _addr;
    }

    sendBuf[0] = slave_addr;             // Set slave address
    sendBuf[1] = cmd;                    // Set command

    sendBuf[2] = (rAddr >> 8) & 0xFF;    // Set high byte of register 'address'
    sendBuf[3] = (rAddr)&0xFF;           // Set low byte =//=

    sendBuf[4] = (val >> 8) & 0xFF;      // Set high byte of register 'value'
    sendBuf[5] = (val)&0xFF;             // Set low byte =//=

    sendBuf[6] = 0x64;
    sendBuf[7] = 0x64;

    UART_Write(UART1, sendBuf, 8);       // Write buffer to energy meter module
}

static void OnUart1ReceivedData(UART_Callback_Param_t param)
{
    memset(data, '0', 26);                  // Set '0's 
    memcpy(data, param.buf, param.length);  // Copy received UART buffer to 'data' variable

    _pzem.voltage = ((uint32_t)data[3] << 8 | (uint32_t)data[4]) / 10.0;                                                            // Resolution 0.1V

    _pzem.current = ((uint32_t)data[5] << 8 | (uint32_t)data[6] | (uint32_t)data[7] << 24 | (uint32_t)data[8] << 16) / 1000.0;      // Resolution 0.001A

    _pzem.power = ((uint32_t)data[9] << 8 | (uint32_t)data[10] | (uint32_t)data[11] << 24 | (uint32_t)data[12] << 16) / 10.0;       // Resolution 0.1W

    _pzem.frequency = ((uint32_t)data[17] << 8 | (uint32_t)data[18]) / 10.0;                                                        // Resolution 0.1Hz

    _pzem.energy = ((uint32_t)data[13] << 8 | (uint32_t)data[14] | (uint32_t)data[15] << 24 | (uint32_t)data[16] << 16) / 1000.0;   // Resolution 1Wh

    _pzem.pf = ((uint32_t)data[19] << 8 | (uint32_t)data[20]) / 100.0;                                                              // Resolution 0.01

    _update = 1;
}

static void pzem_004t_MainTask()
{
    UART_Config_t config = {
        .baudRate = UART_BAUD_RATE_9600,
        .dataBits = UART_DATA_BITS_8,
        .stopBits = UART_STOP_BITS_1,
        .parity = UART_PARITY_NONE,
        .rxCallback = OnUart1ReceivedData,
        .useEvent = false,
    };
    UART_Init(UART1, config);
    _addr = PZEM_DEFAULT_ADDR;

    while (1)
    {
        if (config.useEvent == false)
        {
            _update = 0;

            send8bc(CMD_RIR, 0x00, 0x0A, false, 0xFFFF);

            OS_Sleep(1000);
            if (_update == 1)
            {
                Trace(1, "Voltage :%0.1f", _pzem.voltage);

                Trace(1, "Current :%0.1f", _pzem.current);

                Trace(1, "Power :%0.1f", _pzem.power);

                Trace(1, "Frequency :%0.1f", _pzem.frequency);

                Trace(1, "Energy :%0.0f", _pzem.energy);

                Trace(1, "PF :%0.1f", _pzem.pf);

                Trace(1, "------------------------------------------");
            }
            else
            {
                Trace(1, "Error reading PZEM-004T module");

                Trace(1, "------------------------------------------");

                OS_Sleep(3000);
            }
        }
    }
}

void pzem_004t_Main(void)
{
    pzem_004tTaskHandle = OS_CreateTask(pzem_004t_MainTask,
                                   NULL, NULL, MAIN_TASK_STACK_SIZE, MAIN_TASK_PRIORITY, 0, 0, MAIN_TASK_NAME);
    OS_SetUserMainHandle(&pzem_004tTaskHandle);
}
